import { Given, Then } from 'cypress-cucumber-preprocessor/steps';

Given('I open book page', () => {
  cy.visit('/books');
});

Then(`I see {string} in the title`, title => {
  cy.get('[data-test="app-title"]').should('have.text', title);
});
