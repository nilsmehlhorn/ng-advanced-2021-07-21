describe('Books Page', () => {
  describe('Without Stub', () => {
    beforeEach(() => {
      cy.visit('/books');
    });

    it('should display app title', () => {
      cy.get('[data-test="app-title"]').should('have.text', 'BOOK MONKEY');
    });

    it('should increase the number of books', () => {
      const randomISBN = Math.floor(1000000000000 + Math.random() * 900000).toString();
      cy.get('[data-test="book-card"]')
        .as('books')
        .then(books => {
          cy.get('[data-test="new-book-link"]').click();
          cy.get('[data-test="isbn-field-input"]').type(randomISBN);
          cy.get('[data-test="title-field-input"]').type('My New Book');
          cy.get('[data-test="author-field-input"]').type('Max Mustermann');
          cy.get('[data-test="new-book-create"]').click();
          cy.get('@books').should('have.length', books.length + 1);
          cy.request('DELETE', `http://localhost:4730/books/${randomISBN}`);
        });
    });
  });

  describe('Stubbed API', () => {
    beforeEach(() => {
      cy.intercept('http://localhost:4730/books', { fixture: 'books' });
      cy.visit('/');
    });

    it('should display books', () => {
      cy.get('[data-test="book-card"]').should('have.length', 2);
    });
  });
});
