import Cucumber from 'cypress-cucumber-preprocessor';

const configFactory: Cypress.PluginConfig = (on, config) => {
  on('file:preprocessor', Cucumber());
};

export default configFactory;
