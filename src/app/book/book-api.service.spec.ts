import { BookApiService } from './book-api.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { bookNa } from './models';

describe(BookApiService.name, () => {
  let bookApi: BookApiService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [BookApiService]
    });
    bookApi = TestBed.inject(BookApiService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  it('should get all books', done => {
    const mockBooks = [bookNa(), bookNa()]; // 1
    bookApi.getAll().subscribe(
      // 2
      booksFromApi => {
        expect(booksFromApi).toEqual(mockBooks); // 6
        done();
      }
    );
    const req = httpMock.expectOne('http://localhost:4730/books'); // 3
    expect(req.request.method).toEqual('GET'); // 4
    req.flush(mockBooks); // 5
    // sync test is finished here
  });

  it('should get all books (async/await)', async () => {
    const mockBooks = [bookNa(), bookNa()];
    const booksPromise = bookApi.getAll().toPromise();
    const req = httpMock.expectOne('http://localhost:4730/books');
    expect(req.request.method).toEqual('GET');
    req.flush(mockBooks);
    await expectAsync(booksPromise).toBeResolvedTo(mockBooks);
  });

  it('should throw specific error when network lost', done => {
    bookApi.getAll().subscribe(
      books => {
        fail();
      },
      (error: Error) => {
        expect(error.message).toEqual('Sorry, we have connectivity issues');
        done();
      }
    );
    httpMock.expectOne('http://localhost:4730/books').error(new ErrorEvent('Network Error'));
  });

  it('should throw specific error when server error (async/await)', async () => {
    const booksPromise = bookApi.getAll().toPromise();
    httpMock.expectOne('http://localhost:4730/books').flush('Error', {
      status: 500,
      statusText: 'Internal Server Error'
    });
    try {
      await booksPromise;
      fail();
    } catch (error) {
      expect(error.message).toEqual('Sorry, we could not load any books');
    }
  });

  it('should throw specific error when server error (async/await with expectAsync)', async () => {
    const booksPromise = bookApi.getAll().toPromise();
    httpMock.expectOne('http://localhost:4730/books').flush('Error', {
      status: 500,
      statusText: 'Internal Server Error'
    });
    await expectAsync(booksPromise).toBeRejectedWithError('Sorry, we could not load any books');
  });
});
