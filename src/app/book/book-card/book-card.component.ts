import { ChangeDetectionStrategy, Component, DoCheck, Input } from '@angular/core';
import { bookNa } from '../models';

@Component({
  selector: 'ws-book-card',
  templateUrl: './book-card.component.html',
  styleUrls: ['./book-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BookCardComponent implements DoCheck {
  @Input() content = bookNa();

  ngDoCheck() {}
}
