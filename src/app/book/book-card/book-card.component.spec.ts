import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { bookNa } from '../models';
import { RouterTestingModule } from '@angular/router/testing';

import { BookCardComponent } from './book-card.component';
import { ChangeDetectionStrategy, NO_ERRORS_SCHEMA } from '@angular/core';
import { MatCardModule } from '@angular/material/card';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { MockModule } from 'ng-mocks';

fdescribe('BookCardComponent', () => {
  describe('unit', () => {
    it('should create', () => {
      const component = new BookCardComponent();
      expect(component.content.title).toEqual('n/a');
      expect(component.content.abstract).toEqual('n/a');
      expect(component.content.author).toEqual('n/a');
    });
  });

  describe('template', () => {
    let fixture: ComponentFixture<BookCardComponent>;

    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [BookCardComponent],
        imports: [RouterTestingModule, MockModule(MatCardModule)]
        // schemas: [NO_ERRORS_SCHEMA]
      }).overrideComponent(BookCardComponent, {
        // use different CD strategy in test (difference to real app)
        set: { changeDetection: ChangeDetectionStrategy.Default }
      });
      fixture = TestBed.createComponent(BookCardComponent);
      fixture.detectChanges();
    });

    it('should render book', () => {
      fixture.componentInstance.content = {
        ...bookNa(),
        title: 'My Great Book',
        abstract: 'Lorem ipsum',
        numPages: 123,
        cover: 'http://example.com/book.jpg',
        isbn: '1234'
      };
      fixture.detectChanges();

      const titleFixture = fixture.debugElement.query(By.css('[data-test="title"]'));
      const title: HTMLElement = titleFixture.nativeElement;
      expect(title.innerHTML).toEqual('My Great Book');
      const abstract: HTMLElement = fixture.debugElement.query(By.css('#abstract')).nativeElement;
      expect(abstract.innerHTML.trim()).toEqual('Lorem ipsum');
      const numPage: HTMLElement = fixture.debugElement.query(By.css('.numPages')).nativeElement;
      expect(numPage.textContent?.trim()).toEqual('123');
      const img: HTMLImageElement = fixture.debugElement.query(By.css('[data-test="book-image"]')).nativeElement;
      expect(img.src).toEqual('http://example.com/book.jpg');
      const link: HTMLAnchorElement = fixture.debugElement.query(By.css('[data-test="detail-link"]')).nativeElement;
      expect(link.href).toEqual(location.origin + '/1234');
    });
  });

  describe('spectator', () => {
    let spectator: Spectator<BookCardComponent>;
    const createComponent = createComponentFactory({
      component: BookCardComponent,
      imports: [RouterTestingModule, MockModule(MatCardModule)]
    });

    beforeEach(() => {
      spectator = createComponent();
    });

    fit('should render book', () => {
      spectator.setInput({
        content: {
          ...bookNa(),
          title: 'My Other Great Book'
        }
      });
      const title = spectator.query('[data-test="title"]');
      expect(title).toHaveText('My Other Great Book');
    });
  });
});
