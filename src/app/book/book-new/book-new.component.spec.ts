import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { RouterTestingModule } from '@angular/router/testing';
import { BookApiService } from '../book-api.service';
import { BookNewComponent } from './book-new.component';
import { HarnessLoader } from '@angular/cdk/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatFormFieldHarness } from '@angular/material/form-field/testing';
import { MatInputHarness } from '@angular/material/input/testing';

describe(BookNewComponent.name, () => {
  let fixture: ComponentFixture<BookNewComponent>;
  let loader: HarnessLoader;

  beforeEach(() => {
    const bookApiMock = jasmine.createSpyObj<BookApiService>(['create']);
    TestBed.configureTestingModule({
      declarations: [BookNewComponent],
      imports: [ReactiveFormsModule, MatInputModule, MatFormFieldModule, RouterTestingModule, NoopAnimationsModule],
      providers: [{ provide: BookApiService, useValue: bookApiMock }]
    });
    fixture = TestBed.createComponent(BookNewComponent);
    loader = TestbedHarnessEnvironment.loader(fixture);
    fixture.detectChanges();
  });

  it('should show error for ISBN with less than 3 characters', async () => {
    const isbnField = await loader.getHarness(MatFormFieldHarness.with({ selector: '[data-test="isbn-field"]' }));
    const isbnInput = (await isbnField.getControl()) as MatInputHarness;
    await isbnInput.setValue('12');
    await isbnInput.blur();
    const isbnErrors = await isbnField.getTextErrors();
    expect(isbnErrors).toContain('ISBN has to be at least 3 characters long.');
  });
});
