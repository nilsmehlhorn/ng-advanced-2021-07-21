import { createAction, props } from '@ngrx/store';
import { Book } from '../models/book';

export const createBookStart = createAction('[Books] Create', props<{ book: Book }>());
export const createBookComplete = createAction('[Books] Create Complete', props<{ book: Book }>());

export const loadBooksStart = createAction('[Books] Load');
export const loadBooksComplete = createAction('[Books] Load Complete', props<{ books: Book[] }>());

export const removeBookStart = createAction('[Books] Remove', props<{ book: Book }>());
export const removeBookComplete = createAction('[Books] Remove Complete', props<{ book: Book }>());
export const removeBookFailure = createAction('[Books] Remove Failure', props<{ book: Book }>());
