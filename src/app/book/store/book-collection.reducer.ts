import { createReducer, on } from '@ngrx/store';
import { createBookComplete, loadBooksComplete, removeBookFailure, removeBookStart } from './book-collection.actions';
import { BookCollectionSlice } from './book-collection.slice';

const initialState: BookCollectionSlice = {
  entities: []
};

export const bookCollectionReducer = createReducer(
  initialState,
  on(createBookComplete, (state, { book }) => {
    return {
      ...state,
      entities: [...state.entities, book]
    };
  }),
  on(loadBooksComplete, (state, { books }) => ({
    ...state,
    entities: [...state.entities, ...books]
  })),
  on(removeBookStart, (state, action) => ({
    ...state,
    entities: state.entities.filter(book => book.isbn !== action.book.isbn)
  })),
  on(removeBookFailure, (state, action) => ({
    ...state,
    entities: [...state.entities, action.book]
  }))
);
