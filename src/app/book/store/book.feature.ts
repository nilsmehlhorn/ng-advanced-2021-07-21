import { ActionReducerMap, createFeatureSelector } from '@ngrx/store';
import { bookCollectionReducer } from './book-collection.reducer';
import { BookCollectionSlice } from './book-collection.slice';

export interface BookState {
  bookCollection: BookCollectionSlice;
}

export const bookReducers: ActionReducerMap<BookState> = {
  bookCollection: bookCollectionReducer
};

export const bookFeatureKey = 'book';

// state => state.book
export const bookFeature = createFeatureSelector<{ bookCollection: BookCollectionSlice }>(bookFeatureKey);
