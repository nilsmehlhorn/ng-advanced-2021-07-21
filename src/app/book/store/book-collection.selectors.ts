import { createSelector } from '@ngrx/store';
import { bookFeature } from './book.feature';

export const bookCollection = createSelector(bookFeature, feature => feature.bookCollection);

export const entities = createSelector(bookCollection, collection => collection.entities);

export const bookByIsbn = (isbn: string) => createSelector(entities, books => books.find(book => book.isbn === isbn));
