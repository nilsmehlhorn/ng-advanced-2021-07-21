import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, concatMap, exhaustMap, map, mergeMap, tap } from 'rxjs/operators';
import { of } from 'rxjs'
import { BookApiService } from '../book-api.service';
import {
  createBookComplete,
  createBookStart,
  loadBooksComplete,
  loadBooksStart,
  removeBookComplete,
  removeBookFailure,
  removeBookStart
} from './book-collection.actions';

@Injectable()
export class BookCollectionEffects {
  load$ = createEffect(() =>
    this.action$.pipe(
      ofType(loadBooksStart),
      exhaustMap(action => this.bookApi.getAll()),
      map(books => loadBooksComplete({ books }))
    )
  );

  create$ = createEffect(() =>
    this.action$.pipe(
      ofType(createBookStart),
      mergeMap(action => this.bookApi.create(action.book)),
      map(book => {
        // this.router.navigateByUrl('/books');
        return createBookComplete({ book });
      })
    )
  );

  remove$ = createEffect(() =>
    this.action$.pipe(
      ofType(removeBookStart),
      concatMap(action =>
        this.bookApi.delete(action.book.isbn).pipe(
          map(book => removeBookComplete({ book })),
          catchError(() => of(removeBookFailure({ book: action.book })))
        )
      )
    )
  );

  navigateToBooks$ = createEffect(
    () =>
      this.action$.pipe(
        ofType(createBookComplete, removeBookStart),
        tap(() => {
          this.router.navigateByUrl('/books');
        })
      ),
    { dispatch: false }
  );

  constructor(private action$: Actions, private bookApi: BookApiService, private router: Router) {}
}
