import { Book } from '../models/book';

export interface BookCollectionSlice {
  entities: Book[];
}
