import { Component, Input } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatCardModule } from '@angular/material/card';
import { By } from '@angular/platform-browser';
import { RouterTestingModule } from '@angular/router/testing';
import { MockComponent } from 'ng-mocks';
import { of } from 'rxjs';
import { BookApiService } from '../book-api.service';
import { BookCardComponent } from '../book-card/book-card.component';
import { Book, bookNa } from '../models';
import { BookListComponent } from './book-list.component';

@Component({
  selector: 'ws-book-card',
  template: ''
})
class BookCardMockComponent {
  @Input()
  content: Book | undefined;
}

describe(BookListComponent.name, () => {
  let bookApiMock: jasmine.SpyObj<BookApiService>;

  let createComponent: () => ComponentFixture<BookListComponent>;

  beforeEach(() => {
    bookApiMock = jasmine.createSpyObj<BookApiService>(['getAll']);
    TestBed.configureTestingModule({
      declarations: [BookListComponent, MockComponent(BookCardComponent)],
      //   imports: [RouterTestingModule, MatCardModule],
      providers: [{ provide: BookApiService, useValue: bookApiMock }]
    });
    createComponent = () => {
      const fixture = TestBed.createComponent(BookListComponent);
      fixture.detectChanges();
      return fixture;
    };
  });

  it('should render books', () => {
    bookApiMock.getAll.and.returnValue(of([bookNa(), bookNa()]));
    const fixture = createComponent();
    const bookFixtures = fixture.debugElement.queryAll(By.css('[data-test="book-card"]'));
    expect(bookFixtures).toHaveSize(2);
  });
});
