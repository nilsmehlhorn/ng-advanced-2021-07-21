import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { Book } from '../models';
import * as fromBookCollection from '../store/book-collection.selectors';
import { BookCollectionSlice } from '../store/book-collection.slice';
import { bookFeatureKey } from '../store/book.feature';

@Component({
  selector: 'ws-book-list',
  styleUrls: ['./book-list.component.scss'],
  templateUrl: 'book-list.component.html'
})
export class BookListComponent implements OnInit, OnDestroy {
  books: Book[] = [];

  private subscription = Subscription.EMPTY;

  constructor(private store: Store<{ [bookFeatureKey]: { bookCollection: BookCollectionSlice } }>) {}

  ngOnInit() {
    this.subscription = this.store
      .select(fromBookCollection.entities)
      .subscribe(entities => {
        this.books = entities;
      });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  updateBooks() {
    // won't update cards because we're not changing the reference
    // this.books.forEach(book => {
    //   book.title = `[NEW] ${book.title}`;
    // });
    // will update cards because spread operator creates new reference
    this.books = this.books.map(book => {
      return { ...book, title: `[NEW] ${book.title}` };
    });
  }
}
