import { Injectable } from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc';
import { from, Observable, Subject } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class AuthService {
  loggedIn$: Observable<boolean>;

  constructor(private oauth: OAuthService) {
    this.oauth.configure({
      issuer: 'https://idsvr4.azurewebsites.net',
      redirectUri: window.location.origin,
      clientId: 'spa',
      dummyClientSecret: 'secret',
      responseType: 'code',
      scope: 'openid profile email api',
      showDebugInformation: true
    });
    this.loggedIn$ = from(
      this.oauth.loadDiscoveryDocumentAndTryLogin({
        // somehow window.location.search is reset by navigation
        // before lib can pick it up to retrieve code
        customHashFragment: window.location.search
      })
    ).pipe(map(() => this.oauth.hasValidIdToken() && this.oauth.hasValidAccessToken()));
  }

  login() {
    this.oauth.initLoginFlow();
  }

  logout() {
    this.oauth.revokeTokenAndLogout();
  }
}
