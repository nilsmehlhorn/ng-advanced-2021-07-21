import { Component } from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc';

@Component({
  selector: 'ws-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  constructor(private oauth: OAuthService) {
  }
}
